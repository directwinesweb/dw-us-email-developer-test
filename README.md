# Direct Wines - Email Developer Test

The purpose of this test is to undertand if the candidate has the skills to build HTML emails for Direct Wines. We have provided two PSD's, one for desktop and one for mobile along with our HTML template.

### What is contained within this repo?

- WSJ_Desktop_Template5.psd
- WSJ_Mobile_Template5.psd
- Template5.html
- README.MD

### Rules for the test

- Recreate the email in html as you see in the PSD
- Code email to be responsive
- Slice images in Photoshop using best practices for image optimization
- Test email using a free trial of [Litmus](https://www.litmus.com/) or [Email on Acid](https://www.emailonacid.com/)
- Ensure email looks consistent across popular email clients:
  - Microsoft Outlook (all versions)
  - Gmail
  - Apple Mail
- Supply a link or a PDF of testing results
- Supply a zipped file with html file and an image folder
